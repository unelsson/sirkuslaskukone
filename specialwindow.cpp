#include "specialwindow.hpp"
#include "mainwindow.hpp"

#include <iostream>

#include <QDialog>
#include <QDoubleSpinBox>
#include <QFormLayout>
#include <QLabel>
#include <QPushButton>
#include <QTextEdit>
#include <QVBoxLayout>

#include "billcreator.hpp"

void SpecialWindow::createStuff()
{
    std::cout << "creating..." << std::endl;
    BillCreator creator;
    int counter = 0;

    std::vector<std::string> stdStringDescriptions;
    for(auto editor : mDescriptionEditors) {
        std::string description = editor->toPlainText().toStdString();
        stdStringDescriptions.emplace_back(description);
    }

    std::vector<float> floatSums;
    for(auto editor : mSumEditors) {
        float sum = editor->value() * 100.0f;
        floatSums.emplace_back(sum);
    }

    for(auto customerData : mCustomerData) {
        creator.createBillWithDescriptionAndSum(customerData, stdStringDescriptions, floatSums);
        counter++;
    }
}

SpecialWindow::SpecialWindow(MainWindow* parent, Qt::WindowFlags flags)
    : QDialog(parent, flags)
{
    setWindowTitle("Set billing details:");

    mCreateRowButton = new QPushButton(tr("Add row"), this);
    connect(mCreateRowButton, &QPushButton::released, this, &SpecialWindow::createRow);

    mCreateBillsButton = new QPushButton(tr("Create pdf bills!"), this);
    connect(mCreateBillsButton, &QPushButton::released, this, &SpecialWindow::createStuff);

    QVBoxLayout* mainLayout = new QVBoxLayout;
    mFormLayout = new QFormLayout;

    mainLayout->addLayout(mFormLayout);
    mainLayout->addWidget(mCreateRowButton);
    mainLayout->addWidget(mCreateBillsButton);

    setLayout(mainLayout);
}

void SpecialWindow::createRow()
{
    QLabel* descriptionLabel = new QLabel(tr("Description:"));
    QTextEdit* descriptionEdit = new QTextEdit(this);

    QLabel* sumLabel = new QLabel(tr("Sum:"));
    QDoubleSpinBox* sumEdit = new QDoubleSpinBox(this);
    sumEdit->setRange(0.00, 9999.0);

    mDescriptionEditors.emplace_back(descriptionEdit);
    mSumEditors.emplace_back(sumEdit);

    mFormLayout->addRow(descriptionLabel, descriptionEdit);
    mFormLayout->addRow(sumLabel, sumEdit);
}

void SpecialWindow::loadCustomerData(std::vector<std::vector<std::string>> test)
{
    mCustomerData = test;
}
