#include <vector>

#include <QDialog>
#include <QFileDialog>
#include <QMainWindow>
#include <QMenuBar>
#include <QVector>

#include "billcreator.hpp"
#include "mainwindow.hpp"
#include "specialwindow.hpp"

MainWindow::MainWindow(QWidget* parent, Qt::WindowFlags flags)
    : QMainWindow(parent, flags)
{
    setObjectName("MainWindow");
    setWindowTitle("Laskuttelija");

    createMenus();

    mCentralData = new QTableWidget(this);
    setCentralWidget(mCentralData);

    mSpecialWindow = new SpecialWindow(this);
    connect(this, &MainWindow::sendCustomerData, mSpecialWindow, &SpecialWindow::loadCustomerData);
}

std::vector<std::vector<std::string>> MainWindow::getSelectedCustomerData()
{
    std::vector<std::vector<std::string>> customersData;

    QModelIndexList selection = mCentralData->selectionModel()->selectedRows();
    for(int i = 0; i < selection.count(); i++) {
        QModelIndex index = selection.at(i);
        std::vector<std::string> customerData;
        for(int j = 0; j < mCentralData->columnCount(); ++j) {
            std::string data = "";
            if(mCentralData->item(index.row(), j))
                data = mCentralData->item(index.row(), j)->text().toStdString();
            customerData.emplace_back(data);
        }
        customersData.emplace_back(customerData);
    }
    return customersData;
}

void MainWindow::createSpecialWindow()
{
    std::vector<std::vector<std::string>> selectedCustomers = getSelectedCustomerData();
    emit sendCustomerData(selectedCustomers);
    mSpecialWindow->show();
}

void MainWindow::createBills()
{
    BillCreator creator;
    std::vector<std::vector<std::string>> selectedCustomers = getSelectedCustomerData();
    creator.createBills(selectedCustomers);
}

void MainWindow::openFile()
{
    BillCreator creator;
    std::vector<std::vector<std::string>> allCustomerData;

    QString filename =
        QFileDialog::getOpenFileName(this, tr("Open billing document"), "", tr("comma separated tables (*.csv)"));

    if(!creator.getCustomerData(allCustomerData, filename))
        return;

    mCentralData->setRowCount(allCustomerData.size());
    mCentralData->setColumnCount(allCustomerData.front().size());
    mCentralData->setSortingEnabled(false);

    mCentralData->setHorizontalHeaderItem(0, new QTableWidgetItem(tr("Viite")));
    mCentralData->setHorizontalHeaderItem(1, new QTableWidgetItem(tr("Laskutuskoodi")));
    mCentralData->setHorizontalHeaderItem(2, new QTableWidgetItem(tr("krt/vko")));
    mCentralData->setHorizontalHeaderItem(3, new QTableWidgetItem(tr("Oppilas")));
    mCentralData->setHorizontalHeaderItem(4, new QTableWidgetItem(tr("Ryhmä")));
    mCentralData->setHorizontalHeaderItem(5, new QTableWidgetItem(tr("Laskutettava")));

    int row = 0;
    int column = 0;
    bool copyFlag = false;
    bool empty = true;
    for(auto thisCustomer : allCustomerData) {
        column = 0;
        copyFlag = false;
        empty = true;
        for(auto data : thisCustomer) {
            QString qStringData = QString::fromStdString(data);
            if(data != "")
                empty = false;
            if(column == 3 && data != "")
                copyFlag = true;
            if(column == 5 && data == "" && copyFlag)
                qStringData = mCentralData->item(row, 3)->text(); // tr("COPY");//
            QTableWidgetItem* newItem = new QTableWidgetItem(qStringData);
            if(column == 2)
                newItem->setFlags((Qt::ItemFlag) ~(Qt::ItemIsEnabled)); // disable unimplemented feature
            if(!empty)
                mCentralData->setItem(row, column, newItem);
            column++;
        }
        if(!empty)
            row++;
    }

    mCentralData->setRowCount(row + 1);

    mCentralData->setSortingEnabled(true);
    mCentralData->sortItems(5); // sort by adult name

    mCentralData->setSelectionBehavior(QAbstractItemView::SelectRows);
}

void MainWindow::createMenus()
{
    QMenu* fileMenu = menuBar()->addMenu(tr("&File"));

    const QIcon createIcon = QIcon::fromTheme("document-create", QIcon(":/images/open.png"));
    QAction* createAct = new QAction(createIcon, tr("&Create bills"), this);
    createAct->setStatusTip(tr("Create bills"));
    connect(createAct, &QAction::triggered, this, &MainWindow::createBills);
    fileMenu->addAction(createAct);

    const QIcon createSpecialIcon = QIcon::fromTheme("document-create", QIcon(":/images/open.png"));
    QAction* createSpecialAct = new QAction(createSpecialIcon, tr("&Create special bills to selected"), this);
    createSpecialAct->setStatusTip(tr("Create special bills to selected"));
    connect(createSpecialAct, &QAction::triggered, this, &MainWindow::createSpecialWindow);
    fileMenu->addAction(createSpecialAct);

    const QIcon openIcon = QIcon::fromTheme("document-open", QIcon(":/images/open.png"));
    QAction* openAct = new QAction(openIcon, tr("&Open billing document..."), this);
    openAct->setStatusTip(tr("Open an existing file"));
    connect(openAct, &QAction::triggered, this, &MainWindow::openFile);
    fileMenu->addAction(openAct);

    const QIcon exitIcon = QIcon::fromTheme("document-exit", QIcon(":/images/exit.png"));
    QAction* exitAct = new QAction(exitIcon, tr("&Exit"), this);
    exitAct->setStatusTip(tr("Exit application"));
    connect(exitAct, &QAction::triggered, this, &QWidget::close);
    fileMenu->addAction(exitAct);
}
