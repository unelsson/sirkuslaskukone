#include "billcreator.hpp"

#include <iostream>

// Laskuttelija
// GNU GPL 3 or later
// created by Uoti Huotari
#include <iostream>
#include <fstream>
#include <QString>
#include <string>
#include <locale>
#include <vector>
#include <ctime>
#include <podofo/podofo.h>
#include <podofo/podofo-base.h>
#include "./fast-cpp-csv-parser/csv.h"

BillCreator::BillCreator()
{
}

BillCreator::~BillCreator()
{
}

std::string BillCreator::lower(std::string sourceString)
{
    std::locale locale;
    std::string returnString = "";
    for (auto& character : sourceString)
    {
        returnString += std::tolower(character, locale);
    }
    return returnString;
}

std::string BillCreator::asMajorCurrency(int amountWithDecimals)
{
    return std::to_string( static_cast<int>(std::floor(amountWithDecimals / 100)) );
}

std::string BillCreator::asMinorCurrency(int amountWithDecimals)
{
    std::string returnString = std::to_string(amountWithDecimals % 100);
    if ( returnString != "0") return returnString;
    else return std::string ("00");
}

void BillCreator::checkStartWeek(int& startWeek, std::string billingKeys)
{
    std::string vkoMaksuNumeraali1 = "1";
    std::string vkoMaksuNumeraali2 = "2";
    std::string vkoMaksuNumeraali3 = "3";
    std::string vkoMaksuNumeraali4 = "4";
    std::string vkoMaksuNumeraali5 = "5";
    std::string vkoMaksuNumeraali6 = "6";
    std::string vkoMaksuNumeraali7 = "7";
    std::string vkoMaksuNumeraali8 = "8";
    std::string vkoMaksuNumeraali9 = "9";
    std::string vkoMaksuNumeraali0 = "0";
    std::size_t foundNumber0 = lower(billingKeys).find(vkoMaksuNumeraali0);
    std::size_t foundNumber1 = lower(billingKeys).find(vkoMaksuNumeraali1);
    std::size_t foundNumber2 = lower(billingKeys).find(vkoMaksuNumeraali2);
    std::size_t foundNumber3 = lower(billingKeys).find(vkoMaksuNumeraali3);
    std::size_t foundNumber4 = lower(billingKeys).find(vkoMaksuNumeraali4);
    std::size_t foundNumber5 = lower(billingKeys).find(vkoMaksuNumeraali5);
    std::size_t foundNumber6 = lower(billingKeys).find(vkoMaksuNumeraali6);
    std::size_t foundNumber7 = lower(billingKeys).find(vkoMaksuNumeraali7);
    std::size_t foundNumber8 = lower(billingKeys).find(vkoMaksuNumeraali8);
    std::size_t foundNumber9 = lower(billingKeys).find(vkoMaksuNumeraali9);
    std::vector<std::size_t> foundNumbers;
    foundNumbers.emplace_back(foundNumber0);
    foundNumbers.emplace_back(foundNumber1);
    foundNumbers.emplace_back(foundNumber2);
    foundNumbers.emplace_back(foundNumber3);
    foundNumbers.emplace_back(foundNumber4);
    foundNumbers.emplace_back(foundNumber5);
    foundNumbers.emplace_back(foundNumber6);
    foundNumbers.emplace_back(foundNumber7);
    foundNumbers.emplace_back(foundNumber8);
    foundNumbers.emplace_back(foundNumber9);

    bool foundNumber = false;
    bool dualNumber = false;
    int secondNum = 0;
    for (int i = 0; i < 10; ++i)
    {
        if (foundNumber == true && foundNumbers[i] != std::string::npos)
        {
            secondNum = i;
            dualNumber = true;
        }
        if (foundNumber == false && foundNumbers[i] != std::string::npos)
        {
            startWeek = i;
            foundNumber = true;
        }
    }
    if (dualNumber == true)
    {
        if (foundNumbers[startWeek] > foundNumbers[secondNum])
            startWeek = secondNum * 10 + startWeek;
        else
            startWeek = startWeek * 10 + secondNum;
    }
}

bool BillCreator::hasMultipleEntries(std::vector<std::string> customerData, std::vector<std::vector<std::string>> allCustomers)
{
    bool foundOnce = false;
    bool foundTwice = false; // twice or more

    // check for student name first
    for (const auto& customer : allCustomers)
    {
        if (foundOnce == true && lower(customer[3]) == lower(customerData[3]))
        {
            foundTwice = true;
        }
        if (foundOnce == false && lower(customer[3]) == lower(customerData[3]))
        {
            foundOnce = true;
        }
    }

    foundOnce = false;

    if (customerData[5] == "") return foundTwice;

    // check for parent name also
    for (const auto& customer : allCustomers)
    {
        if (foundOnce == true && lower(customer[5]) == lower(customerData[5]))
        {
            foundTwice = true;
        }
        if (foundOnce == false && lower(customer[5]) == lower(customerData[5]))
        {
            foundOnce = true;
        }
    }

    return foundTwice;
}

std::vector<std::string> BillCreator::getChildList(std::string parentName, const std::vector<std::vector<std::string>>& allCustomers)
{
    if (parentName == "")
    {
        std::vector<std::string> emptyList;
        return emptyList;
    }
    std::vector<std::string> childList;
    for (const auto& customer : allCustomers)
    {
        if (lower(customer[5]) == lower(parentName))
        {
            childList.emplace_back(customer[3]);
        }
    }
    sort( childList.begin(), childList.end() );
    childList.erase( std::unique( childList.begin(), childList.end() ), childList.end() );
    return childList;
}

// gets a list of groups per once participant, first group full price, other groups discounted
std::vector<std::pair<int, int>> BillCreator::getSortedListOfGroups(std::string customerName, const std::vector<std::vector<std::string>>& priceData, const std::vector<std::vector<std::string>>& allCustomers)
{
    std::vector<int> entryNumbers;
    for (int i = 0; i < allCustomers.size(); ++i)
    {
        if (allCustomers[i][3] == customerName)
        {
            entryNumbers.emplace_back(i);
        }
    }


    std::vector<std::pair<int, int>> groupPrices; // <index, price>
    for (const auto& index : entryNumbers)
    {
        std::string tunninRyhma = "t";
        std::string puolentoistaTunninRyhma = "p";
        std::string muuRyhma = "m";
        std::size_t foundTunninRyhma =  lower(allCustomers[index][1]).find(tunninRyhma);
        std::size_t foundPuolentoistaTunninRyhma = lower(allCustomers[index][1]).find(puolentoistaTunninRyhma);
        std::size_t foundMuuRyhma = lower(allCustomers[index][1]).find(muuRyhma);

        // Make a list of groups per full price
        if (foundTunninRyhma != std::string::npos)
        {
            int fullPayment = 0;
            for (const auto& price : priceData)
            {
                if (price[0] == "60min") fullPayment = std::stof(price[1]);
            }
            groupPrices.emplace_back(std::make_pair(index, fullPayment));
        }

        if (foundPuolentoistaTunninRyhma != std::string::npos)
        {
            int fullPayment = 0;
            for (const auto& price : priceData)
            {
                if (price[0] == "90min") fullPayment = std::stof(price[1]);
            }
            groupPrices.emplace_back(std::make_pair(index, fullPayment));
        }
        //if (foundMuuRyhma != std::string::npos) //TODO IMPLEMENT LATER

    }

    //pick the highest priced group as the first one, other groups will have discount price
    std::sort(groupPrices.begin(), groupPrices.end(), [](auto &left, auto &right) {
        return left.second > right.second; });
    bool firstGroup = false;
    for (auto& group : groupPrices)
    {
        if (firstGroup == false)
        {
            firstGroup = true;
            continue;
        }
        for (const auto& price : priceData)
        {
            if (price[0] == "90min2") group.second = std::stoi(price[1]); // Use just once price for all second lvl groups
        }
    }
    /*for (auto& group : groupPrices)
    {
        //std::cout << allCustomers[group.first][1] << " / " << group.second << std::endl;
    }*/
    return groupPrices;
}

void BillCreator::addSpecialPaymentRow(std::string itemName, float paymentAmount, int rowNumber, int itemRowSpacer, PoDoFo::PdfPainterMM* painter)
{
    painter->DrawTextMM( mColumn1X, mContentMainContent - itemRowSpacer,
    reinterpret_cast<const PoDoFo::pdf_utf8*>( itemName.c_str() ));

    std::string paymentString = asMajorCurrency(paymentAmount) + "." + asMinorCurrency(paymentAmount) + "€";
    painter->DrawTextMM( mColumn2X, mContentMainContent - itemRowSpacer,
    reinterpret_cast<const PoDoFo::pdf_utf8*>( paymentString.c_str() ));

    // VAT 0% is the same as the payment above (VAT over 0 not supported)
    painter->DrawTextMM( mColumn4X, mContentMainContent - itemRowSpacer,
    reinterpret_cast<const PoDoFo::pdf_utf8*>( paymentString.c_str() ));
    
    // Template code to add for adding payments dates for each row
    /*time_t now = time(0) + (24 * 60 * 60) * 14;
    tm *localTime = std::localtime(&now);
    std::string day = std::to_string(localTime->tm_mday);
    std::string month = std::to_string(1 + localTime->tm_mon);
    std::string year = std::to_string(1900 + localTime->tm_year);

    std::string dateExpired = day + "." + month + "." + year;
    painter.DrawTextMM( mColumn3X, mContentMainContent - itemRowSpacer,
    reinterpret_cast<const PoDoFo::pdf_utf8*>(dateExpired.c_str()) );*/
}

void BillCreator::createBillWithDescriptionAndSum(const std::vector<std::string>& customerData, std::vector<std::string> descriptions, std::vector<float> paymentAmounts, std::string addToFileName)
{
    // Markers how the spreadsheet is interpreted
    std::string underscoresName = customerData[5];
    if (underscoresName == "") underscoresName = customerData[3]; // If no parent name, use participant name
    std::replace(underscoresName.begin(), underscoresName.end(), ' ', '_');

    std::string fileName = "Lasku_" + underscoresName + addToFileName + ".pdf";
    const char* pszFilename = fileName.c_str();

    PoDoFo::PdfStreamedDocument document( pszFilename );
    PoDoFo::PdfPage* pPage;
    PoDoFo::PdfPainterMM painter;

    pPage = document.CreatePage( PoDoFo::PdfPage::CreateStandardPageSize( PoDoFo::ePdfPageSize_A4 ) );
    if( !pPage )
    {
        PODOFO_RAISE_ERROR( PoDoFo::ePdfError_InvalidHandle );
    }

    painter.SetPage( pPage );

    defineFonts(document);

    PoDoFo::PdfImage logoImage(&document);
    const char* logoFilename = "logo.png";
    logoImage.LoadFromFile(logoFilename);

    painter.DrawImageMM( mLogoX, mLogoY, &logoImage, mImageScale, mImageScale);

    painter.SetFont( pBoldFont );
    painter.DrawTextMM( mCaptionXBox1, mCaptionYBox1 + mLineSpacing + 5000, "Lasku" );
    painter.SetFont( pFont );

    io::CSVReader<2, io::trim_chars<>, io::no_quote_escape<';'>, io::throw_on_overflow, io::single_line_comment<'#'> > yleisetrivit("yleisetrivit.csv");
    yleisetrivit.read_header(io::ignore_extra_column, "Caption", "Data");
    std::string caption;
    std::string data;

    int counter = 0;
    while(yleisetrivit.read_row(caption, data))
    {
        painter.DrawTextMM( mCaptionXBox1, mCaptionYBox1 - counter * mLineSpacing,
            reinterpret_cast<const PoDoFo::pdf_utf8*>(caption.c_str()) );
        if (caption == "Päivämäärä")
        {
            time_t now = time(0);
            tm *localTime = std::localtime(&now);
            std::string day = std::to_string(localTime->tm_mday);
            std::string month = std::to_string(1 + localTime->tm_mon);
            std::string year = std::to_string(1900 + localTime->tm_year);

            std::string dateNow = day + "." + month + "." + year;
            painter.DrawTextMM( mContentXBox1, mCaptionYBox1 - counter * mLineSpacing,
                reinterpret_cast<const PoDoFo::pdf_utf8*>(dateNow.c_str()) );
        }
        if (caption == "Eräpäivä")
        {
            time_t now = time(0) + (24 * 60 * 60) * 14;
            tm *localTime = std::localtime(&now);
            std::string day = std::to_string(localTime->tm_mday);
            std::string month = std::to_string(1 + localTime->tm_mon);
            std::string year = std::to_string(1900 + localTime->tm_year);

            std::string dateExpires = day + "." + month + "." + year;
            painter.DrawTextMM( mContentXBox1, mCaptionYBox1 - counter * mLineSpacing,
                reinterpret_cast<const PoDoFo::pdf_utf8*>(dateExpires.c_str()) );
        }
        if (data != "")
        {
            painter.DrawTextMM( mContentXBox1, mCaptionYBox1 - counter * mLineSpacing,
                reinterpret_cast<const PoDoFo::pdf_utf8*>(data.c_str()) );
        }
        if (caption == "Viitenumero")
        {
            painter.DrawTextMM( mContentXBox1, mCaptionYBox1 - counter * mLineSpacing,
                reinterpret_cast<const PoDoFo::pdf_utf8*>(customerData[0].c_str()) );
        }
        counter++;
    }

    painter.SetFont( pCaptionFont );
    std::string captionAndCustomerName = "Lasku / " + customerData[5];
    painter.DrawTextMM( mCaptionXMainContent, mCaptionYMainContent,
        reinterpret_cast<const PoDoFo::pdf_utf8*>(captionAndCustomerName.c_str()));
    painter.SetFont( pFont );

    int itemRowSpacer = 0;
    int totalPayment = 0;

    painter.SetFont( pBoldFont );

    painter.DrawTextMM( mColumn1X, mContentMainContent - itemRowSpacer,
        reinterpret_cast<const PoDoFo::pdf_utf8*>( "Selite" ));
    painter.DrawTextMM( mColumn2X, mContentMainContent - itemRowSpacer,
        reinterpret_cast<const PoDoFo::pdf_utf8*>( "á hinta" ));
    /*painter.DrawTextMM( mColumn3X, mContentMainContent - itemRowSpacer,
        reinterpret_cast<const PoDoFo::pdf_utf8*>( "Eräpäivä" ));*/
    painter.DrawTextMM( mColumn4X, mContentMainContent - itemRowSpacer,
        reinterpret_cast<const PoDoFo::pdf_utf8*>( "Hinta (alv 0%)" ));
    itemRowSpacer += mSpaceBetweenItems + mSpaceBetweenItems;

    painter.SetFont( pFont );

    for (int i = 0; i < paymentAmounts.size(); ++i)
    {        
        addSpecialPaymentRow(descriptions[i], paymentAmounts[i], i, itemRowSpacer, &painter);
        itemRowSpacer = itemRowSpacer + mSpaceBetweenItems;
        totalPayment += paymentAmounts[i];
    }
    
    // Add a final sum row
    painter.SetFont( pBoldFont );
    painter.DrawTextMM( mColumn1X, mContentMainContent - itemRowSpacer,
        reinterpret_cast<const PoDoFo::pdf_utf8*>( "Yhteensä" ));

    std::string paymentString = asMajorCurrency(totalPayment) + "." + asMinorCurrency(totalPayment) + "€";
    painter.DrawTextMM( mColumn4X, mContentMainContent - itemRowSpacer,
        reinterpret_cast<const PoDoFo::pdf_utf8*>( paymentString.c_str() ));
    painter.SetFont( pFont );

    drawFooter(&painter);

    painter.FinishPage();

    document.GetInfo()->SetCreator ( PoDoFo::PdfString("Sirkuslaskukone") );
    document.GetInfo()->SetAuthor  ( PoDoFo::PdfString("-") );
    document.GetInfo()->SetTitle   ( PoDoFo::PdfString("Lukukausimaksu") );
    document.GetInfo()->SetSubject ( PoDoFo::PdfString("Sirkuslaskukoneen tekemä lukukausimaksu") );

    document.Close();
}

void BillCreator::addBillRow(std::string itemName, int paymentAmount, int& totalPayment, int& itemRowSpacer, PoDoFo::PdfPainterMM* painter, int extraPaymentDays)
{
    painter->DrawTextMM( mColumn1X, mContentMainContent - itemRowSpacer,
        reinterpret_cast<const PoDoFo::pdf_utf8*>( itemName.c_str() ));

    totalPayment += paymentAmount;
                std::string paymentString = asMajorCurrency(paymentAmount) + "." + asMinorCurrency(paymentAmount) + "€";

    painter->DrawTextMM( mColumn2X, mContentMainContent - itemRowSpacer,
        reinterpret_cast<const PoDoFo::pdf_utf8*>( paymentString.c_str() ));

    time_t paymentTime = time(0) + (24 * 60 * 60) * (14 + extraPaymentDays);
    tm *localTime = std::localtime(&paymentTime);
    std::string day = std::to_string(localTime->tm_mday);
    std::string month = std::to_string(1 + localTime->tm_mon);
    std::string year = std::to_string(1900 + localTime->tm_year);

    std::string dateExpired = day + "." + month + "." + year;
    painter->DrawTextMM( mColumn3X, mContentMainContent - itemRowSpacer,
        reinterpret_cast<const PoDoFo::pdf_utf8*>(dateExpired.c_str()) );

    painter->SetFont( pBoldFont );
    painter->DrawTextMM( mColumn4X, mContentMainContent - itemRowSpacer,
        reinterpret_cast<const PoDoFo::pdf_utf8*>( paymentString.c_str() ));
    painter->SetFont( pFont );
    itemRowSpacer += mSpaceBetweenItems;
}

void BillCreator::createBill(const std::vector<std::string>& customerData, const std::vector<std::vector<std::string>>& priceData, const std::vector<std::vector<std::string>>& allCustomers)
{
    // Markers how the spreadsheet is interpreted
    std::string firstPaymentMarker = "a";
    std::string secondPaymentMarker = "b";
    std::string tunninRyhma = "t";
    std::string puolentoistaTunninRyhma = "p";
    std::string muuRyhma = "m";
    std::size_t foundFirstPaymentMarker = lower(customerData[1]).find(firstPaymentMarker);
    std::size_t foundSecondPaymentMarker = lower(customerData[1]).find(secondPaymentMarker);
    std::size_t foundTunninRyhma = lower(customerData[1]).find(tunninRyhma);
    std::size_t foundPuolentoistaTunninRyhma = lower(customerData[1]).find(puolentoistaTunninRyhma);
    std::size_t foundMuuRyhma = lower(customerData[1]).find(muuRyhma);

    int startWeek = 0;
    checkStartWeek(startWeek, customerData[1]);

    std::string underscoresName = customerData[5];
    if (underscoresName == "") underscoresName = customerData[3]; // If no parent name, use participant name
    std::replace(underscoresName.begin(), underscoresName.end(), ' ', '_');

    std::string addToFileName = "";

    if (foundFirstPaymentMarker!=std::string::npos) addToFileName = "_osa1";
    if (foundSecondPaymentMarker!=std::string::npos) addToFileName = "_osa2";
    if (foundFirstPaymentMarker!=std::string::npos && foundSecondPaymentMarker!=std::string::npos) addToFileName = "";

    std::string fileName = "Lukukausimaksu_" + underscoresName + addToFileName + ".pdf";
    const char* pszFilename = fileName.c_str();

    PoDoFo::PdfStreamedDocument document( pszFilename );
    PoDoFo::PdfPage* pPage;
    PoDoFo::PdfPainterMM painter;

    pPage = document.CreatePage( PoDoFo::PdfPage::CreateStandardPageSize( PoDoFo::ePdfPageSize_A4 ) );
    if( !pPage )
    {
        PODOFO_RAISE_ERROR( PoDoFo::ePdfError_InvalidHandle );
    }

    painter.SetPage( pPage );

    defineFonts(document);

    PoDoFo::PdfImage logoImage(&document);
    const char* logoFilename = "logo.png";
    logoImage.LoadFromFile(logoFilename);

    painter.DrawImageMM( mLogoX, mLogoY, &logoImage, mImageScale, mImageScale);

    painter.SetFont( pBoldFont );
    painter.DrawTextMM( mCaptionXBox1, mCaptionYBox1 + mLineSpacing + 5000, "Lasku" );
    painter.SetFont( pFont );

    io::CSVReader<2, io::trim_chars<>, io::no_quote_escape<';'>, io::throw_on_overflow, io::single_line_comment<'#'> > yleisetrivit("yleisetrivit.csv");
    yleisetrivit.read_header(io::ignore_extra_column, "Caption", "Data");
    std::string caption;
    std::string data;

    int counter = 0;
    while(yleisetrivit.read_row(caption, data))
    {
        painter.DrawTextMM( mCaptionXBox1, mCaptionYBox1 - counter * mLineSpacing,
            reinterpret_cast<const PoDoFo::pdf_utf8*>(caption.c_str()) );
        if (caption == "Päivämäärä")
        {
            time_t now = time(0);
            tm *localTime = std::localtime(&now);
            std::string day = std::to_string(localTime->tm_mday);
            std::string month = std::to_string(1 + localTime->tm_mon);
            std::string year = std::to_string(1900 + localTime->tm_year);

            std::string dateNow = day + "." + month + "." + year;
            painter.DrawTextMM( mContentXBox1, mCaptionYBox1 - counter * mLineSpacing,
                reinterpret_cast<const PoDoFo::pdf_utf8*>(dateNow.c_str()) );
        }
        if (caption == "Eräpäivä")
        {
            time_t now = time(0) + (24 * 60 * 60) * 14;
            tm *localTime = std::localtime(&now);
            std::string day = std::to_string(localTime->tm_mday);
            std::string month = std::to_string(1 + localTime->tm_mon);
            std::string year = std::to_string(1900 + localTime->tm_year);

            std::string dateExpires = day + "." + month + "." + year;
            painter.DrawTextMM( mContentXBox1, mCaptionYBox1 - counter * mLineSpacing,
                reinterpret_cast<const PoDoFo::pdf_utf8*>(dateExpires.c_str()) );
        }
        if (data != "")
        {
            painter.DrawTextMM( mContentXBox1, mCaptionYBox1 - counter * mLineSpacing,
                reinterpret_cast<const PoDoFo::pdf_utf8*>(data.c_str()) );
        }
        if (caption == "Viitenumero")
        {
            painter.DrawTextMM( mContentXBox1, mCaptionYBox1 - counter * mLineSpacing,
                reinterpret_cast<const PoDoFo::pdf_utf8*>(customerData[0].c_str()) );
        }
        counter++;
    }

    painter.SetFont( pCaptionFont );
    painter.DrawTextMM( mCaptionXMainContent, mCaptionYMainContent,
        reinterpret_cast<const PoDoFo::pdf_utf8*>("Lukukausimaksu"));
    painter.SetFont( pFont );

    int itemRowSpacer = 0;

    painter.SetFont( pBoldFont );

    painter.DrawTextMM( mColumn1X, mContentMainContent - itemRowSpacer,
        reinterpret_cast<const PoDoFo::pdf_utf8*>( "Selite" ));
    painter.DrawTextMM( mColumn2X, mContentMainContent - itemRowSpacer,
        reinterpret_cast<const PoDoFo::pdf_utf8*>( "á hinta" ));
    painter.DrawTextMM( mColumn3X, mContentMainContent - itemRowSpacer,
        reinterpret_cast<const PoDoFo::pdf_utf8*>( "Eräpäivä" ));
    painter.DrawTextMM( mColumn4X, mContentMainContent - itemRowSpacer,
        reinterpret_cast<const PoDoFo::pdf_utf8*>( "Hinta (alv 0%)" ));
    itemRowSpacer += mSpaceBetweenItems + mSpaceBetweenItems;

    painter.SetFont( pFont );

    int totalPayment = 0; //This is calculated by entries
    int paymentAmount = 0; // Price per item
    int fullPayment = 0; // Full payment price of one entry

    std::vector<std::string> childList = getChildList(customerData[5], allCustomers);
    if (childList.empty())
    {
        std::cout << "Virhe laskun luomisessa oppilaalle " << customerData[3] << ", huoltajalle " << customerData[5] << std::endl;
    }
    std::cout << "\033[1;31m" << "\tHavaittu monimutkainen lasku henkilölle " << customerData[5] << "\033[0m" << std::endl;
    for (const auto customerName : childList)
    {
        std::cout << "\033[1;32m" << "\tLisätään laskutus oppilaasta " << customerName << "\033[0m" << std::endl;
        
        std::vector<std::pair<int, int>> sortedGroupList = getSortedListOfGroups(customerName, priceData, allCustomers);
        
        for (const auto& group : sortedGroupList)
        {
            std::size_t foundFirstPaymentMarker = lower(allCustomers[group.first][1]).find(firstPaymentMarker);
            std::size_t foundSecondPaymentMarker = lower(allCustomers[group.first][1]).find(secondPaymentMarker);
            if (foundFirstPaymentMarker != std::string::npos)
            {
                //addBillRow(std::string itemName, int paymentAmount, int& totalPayment, int& itemRowSpacer, PoDoFo::PdfPainterMM* painter)
                addBillRow("1. erä / " + allCustomers[group.first][4] + ", " + allCustomers[group.first][3], group.second / 2, totalPayment, itemRowSpacer, &painter);
            }            
            if (foundSecondPaymentMarker!= std::string::npos)
            {
                int extraPaymentDays = 30;
                addBillRow("2. erä / " + allCustomers[group.first][4] + ", " + allCustomers[group.first][3], group.second / 2, totalPayment, itemRowSpacer, &painter, extraPaymentDays);
            }            
        }
    }

    // Yhteensä
    itemRowSpacer += mSpaceBetweenItems;
    painter.SetFont( pBoldFont );
    painter.DrawTextMM( mColumn1X, mContentMainContent - itemRowSpacer,
        reinterpret_cast<const PoDoFo::pdf_utf8*>("Yhteensä"));
    painter.SetFont( pFont );

    std::string paymentString = asMajorCurrency(totalPayment) + "." + asMinorCurrency(totalPayment) + "€";

    painter.SetFont( pBoldFont );
    painter.DrawTextMM( mColumn4X, mContentMainContent - itemRowSpacer,
        reinterpret_cast<const PoDoFo::pdf_utf8*>( paymentString.c_str() ));
    painter.SetFont( pFont );

    drawFooter(&painter);

    painter.FinishPage();

    document.GetInfo()->SetCreator ( PoDoFo::PdfString("Sirkuslaskukone") );
    document.GetInfo()->SetAuthor  ( PoDoFo::PdfString("-") );
    document.GetInfo()->SetTitle   ( PoDoFo::PdfString("Lukukausimaksu") );
    document.GetInfo()->SetSubject ( PoDoFo::PdfString("Sirkuslaskukoneen tekemä lukukausimaksu") );

    document.Close();
}

void BillCreator::drawFooter(PoDoFo::PdfPainterMM* painter)
{
    std::vector<std::string> footerData;
    getFooterData(footerData);
    
    int itemRowSpacer = 0;
    
    painter->SetFont( pItalicFont );
    painter->DrawTextMM( mCaptionXFooter, mCaptionYFooter,
        reinterpret_cast<const PoDoFo::pdf_utf8*>(footerData[12].c_str()));
    itemRowSpacer += mLineSpacing;
    painter->DrawTextMM( mCaptionXFooter, mCaptionYFooter - itemRowSpacer,
        reinterpret_cast<const PoDoFo::pdf_utf8*>(footerData[13].c_str()));

    painter->SetFont( pBoldFont );
    itemRowSpacer += mSpaceBetweenItems;
    painter->DrawTextMM( mCaptionXFooter, mCaptionYFooter - itemRowSpacer,
        reinterpret_cast<const PoDoFo::pdf_utf8*>(footerData[1].c_str()));
    painter->DrawTextMM( mColumn2X, mCaptionYFooter - itemRowSpacer,
        reinterpret_cast<const PoDoFo::pdf_utf8*>(footerData[2].c_str()));
    painter->DrawTextMM( mColumn3bX, mCaptionYFooter - itemRowSpacer,
        reinterpret_cast<const PoDoFo::pdf_utf8*>(footerData[3].c_str()));

    painter->SetFont( pBoldFont );
    itemRowSpacer += mSpaceBetweenItems;
    painter->DrawTextMM( mCaptionXFooter, mCaptionYFooter - itemRowSpacer,
        reinterpret_cast<const PoDoFo::pdf_utf8*>(footerData[0].c_str()));
    painter->SetFont( pFont );
    painter->DrawTextMM( mColumn2X, mCaptionYFooter - itemRowSpacer,
        reinterpret_cast<const PoDoFo::pdf_utf8*>(footerData[6].c_str()));
    painter->DrawTextMM( mColumn3bX, mCaptionYFooter - itemRowSpacer,
        reinterpret_cast<const PoDoFo::pdf_utf8*>(footerData[7].c_str()));

    itemRowSpacer += mLineSpacing;
    painter->DrawTextMM( mCaptionXFooter, mCaptionYFooter - itemRowSpacer,
        reinterpret_cast<const PoDoFo::pdf_utf8*>(footerData[4].c_str()));
    painter->DrawTextMM( mColumn2X, mCaptionYFooter - itemRowSpacer,
        reinterpret_cast<const PoDoFo::pdf_utf8*>(footerData[8].c_str()));
    painter->DrawTextMM( mColumn3bX, mCaptionYFooter - itemRowSpacer,
        reinterpret_cast<const PoDoFo::pdf_utf8*>(footerData[10].c_str()));

    itemRowSpacer += mLineSpacing;
    painter->DrawTextMM( mCaptionXFooter, mCaptionYFooter - itemRowSpacer,
        reinterpret_cast<const PoDoFo::pdf_utf8*>(footerData[5].c_str()));
    painter->DrawTextMM( mColumn2X, mCaptionYFooter - itemRowSpacer,
        reinterpret_cast<const PoDoFo::pdf_utf8*>(footerData[9].c_str()));

    itemRowSpacer += mBiggerSpacer;
    painter->DrawTextMM( mCaptionXFooter, mCaptionYFooter - itemRowSpacer,
        reinterpret_cast<const PoDoFo::pdf_utf8*>(footerData[11].c_str()));
}

void BillCreator::defineFonts(PoDoFo::PdfStreamedDocument& document)
{
    pFont = document.CreateFont( "Verdana" );
    if( !pFont )
    {
        PODOFO_RAISE_ERROR( PoDoFo::ePdfError_InvalidHandle );
    }
    pFont->SetFontSize( 8.0f );
    pFont->EmbedFont();

    pCaptionFont = document.CreateFont( "Verdana" , true, false);
    if( !pCaptionFont )
    {
        PODOFO_RAISE_ERROR( PoDoFo::ePdfError_InvalidHandle );
    }
    pCaptionFont->SetFontSize( 18.0f );
    pCaptionFont->EmbedFont();

    pBoldFont = document.CreateFont( "Verdana" , true, false);
    if( !pBoldFont )
    {
        PODOFO_RAISE_ERROR( PoDoFo::ePdfError_InvalidHandle );
    }
    pBoldFont->SetFontSize( 8.0f );
    pBoldFont->EmbedFont();

    pItalicFont = document.CreateFont( "Verdana" , false, true);
    if( !pItalicFont )
    {
        PODOFO_RAISE_ERROR( PoDoFo::ePdfError_InvalidHandle );
    }
    pItalicFont->SetFontSize( 8.0f );
    pItalicFont->EmbedFont();
}

void BillCreator::getFooterData(std::vector<std::string> &footerData)
{
    // NAME BANK IBAN BIC ADDRESS ADDRESS2 PHONE EMAIL WEBPAGE
    io::CSVReader<14, io::trim_chars<>, io::no_quote_escape<';'>, io::throw_on_overflow, io::single_line_comment<'#'> > footerdatafile("footerstuff.csv");
    footerdatafile.read_header(io::ignore_extra_column, "NAME", "BANK", "IBAN", "BIC", "ADDRESS", "ADDRESS2", "CITY", "PHONE", "EMAIL", "WEBPAGE", "COMPANYID", "EXTRA", "PREEXTRA", "PREEXTRA2");
    
    std::string name, bank, iban, bic, address, addressTwo, city,phoneNumber, email, webpage, companyid, extra, preExtra, preExtraTwo;
    while(footerdatafile.read_row(name, bank, iban, bic, address, addressTwo, city, phoneNumber, email, webpage, companyid, extra, preExtra, preExtraTwo))
    {
        footerData.emplace_back(name);
        footerData.emplace_back(bank);
        footerData.emplace_back(iban);
        footerData.emplace_back(bic);
        footerData.emplace_back(address);
        footerData.emplace_back(addressTwo);
        footerData.emplace_back(city);
        footerData.emplace_back(phoneNumber);
        footerData.emplace_back(email);
        footerData.emplace_back(webpage);
        footerData.emplace_back(companyid);
        footerData.emplace_back(extra);
        footerData.emplace_back(preExtra);
        footerData.emplace_back(preExtraTwo);
    }
}

bool BillCreator::getCustomerData(std::vector<std::vector<std::string>> &customerData, const QString& filename)
{
    // Viite, laskutus, ryhmien lukumäärä, oppilaan nimi, syntymävuosi, ryhmän nimi, huoltajan nimi, sähköposti, puhelinnumero, muuta
    try 
    {
        io::CSVReader<6, io::trim_chars<>, io::no_quote_escape<';'>, io::throw_on_overflow, io::single_line_comment<'#'> > laskuaineisto(filename.toStdString().c_str());
        laskuaineisto.read_header(io::ignore_extra_column, "VIITE", "LASKUTUS", "RYHMIEN LUKUMÄÄRÄ", "OPPILAAN NIMI", "RYHMÄN NIMI", "HUOLTAJAN NIMI");
        std::string viite;
        std::string laskutus;
        std::string ryhmienlukumaara;
        std::string oppilaannimi;
        std::string ryhmannimi;
        std::string huoltajannimi;

        while(laskuaineisto.read_row(viite, laskutus, ryhmienlukumaara, oppilaannimi, ryhmannimi, huoltajannimi))
        {
            std::vector<std::string> customerDataRow;
            customerDataRow.emplace_back(viite);
            customerDataRow.emplace_back(laskutus);
            customerDataRow.emplace_back(ryhmienlukumaara);
            customerDataRow.emplace_back(oppilaannimi);
            customerDataRow.emplace_back(ryhmannimi);
            customerDataRow.emplace_back(huoltajannimi);
            customerData.emplace_back(customerDataRow);
        }
        return true;
    }
    catch (const std::exception& exception)
    {
        std::cout << "Error in opening the billing document: " << exception.what() << std::endl;
        return false;
    }
}

void BillCreator::getPriceData(std::vector<std::vector<std::string>> &priceData)
{
    // 90min, 60min, 90min2, 60min2, aloitusviikko, kertamaksu, perhesirkus60_Xhlö, perhesirkus90_Xhlö
    io::CSVReader<2, io::trim_chars<>, io::no_quote_escape<';'>, io::throw_on_overflow, io::single_line_comment<'#'> > hintaaineisto("hinnat.csv");
    hintaaineisto.read_header(io::ignore_extra_column, "CAPTION", "PRICE");
    std::string caption;
    std::string price;

    std::vector<std::string> priceDataRow;

    while(hintaaineisto.read_row(caption, price))
    {
        priceDataRow.clear();
        priceDataRow.emplace_back(caption);
        priceDataRow.emplace_back(price);
        priceData.emplace_back(priceDataRow);
    }
}

int BillCreator::createBills (std::vector<std::vector<std::string>> allCustomers) 
{
    std::vector<std::vector<std::string>> priceData;
    std::vector<std::string> doneCustomers;
    try {
        getPriceData(priceData);
        for (const auto& customer : allCustomers)
        {
            bool alreadyCreated = false;
            if (customer[3] == "" || customer[1] == "") continue;
            for (const auto& alreadyDoneCustomer : doneCustomers)
            {
                if (customer[5] == alreadyDoneCustomer)
                {
                    alreadyCreated = true;
                }
                if (customer[3] == alreadyDoneCustomer)
                {
                    alreadyCreated = true;
                }
            }
            if (!alreadyCreated)
            {
                std::cout << "Luodaan lasku oppilaalle " << customer[3] << " -> huoltajalle " << customer[5] << std::endl;
                createBill(customer, priceData, allCustomers);
                doneCustomers.emplace_back(customer[3]);
                doneCustomers.emplace_back(customer[5]);
            }
        }
        return 1;
    } catch ( const PoDoFo::PdfError & eCode ) {
        eCode.PrintErrorMsg();
        return eCode.GetError();
    }


    return 0;
}