#ifndef SPECIALWINDOW_HPP
#define SPECIALWINDOW_HPP

#include <QDialog>
#include <QDoubleSpinBox>
#include <QFormLayout>
#include <QPushButton>
#include <QString>
#include <QTextEdit>
#include <QVector>
#include <vector>

#include "mainwindow.hpp"

class SpecialWindow : public QDialog
{
    Q_OBJECT

public:
    SpecialWindow(MainWindow* parent = nullptr, Qt::WindowFlags flags = Qt::WindowFlags());
    virtual ~SpecialWindow() {};

private:
    QFormLayout* mFormLayout;

    std::vector<QTextEdit*> mDescriptionEditors;
    std::vector<QDoubleSpinBox*> mSumEditors;
    QPushButton* mCreateBillsButton = nullptr;
    QPushButton* mCreateRowButton = nullptr;
    std::vector<std::vector<std::string>> mCustomerData;

public slots:
    void createRow();
    void createStuff();
    void loadCustomerData(std::vector<std::vector<std::string>> test);
};

#endif // SPECIALWINDOW_HPP
