#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QMainWindow>
#include <QTableWidget>

class SpecialWindow;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget* parent = nullptr, Qt::WindowFlags flags = Qt::WindowFlags());
    virtual ~MainWindow() {};

    std::vector<std::vector<std::string>> getSelectedCustomerData();

private:
    void createSpecialWindow();
    void createBills();
    void openFile();
    void createMenus();

    QTableWidget* mCentralData = nullptr;
    SpecialWindow* mSpecialWindow = nullptr;

signals:
    void sendCustomerData(std::vector<std::vector<std::string>> test);
};

#endif // MAINWINDOW_HPP
