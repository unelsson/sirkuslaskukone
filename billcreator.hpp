#ifndef BILLCREATOR_HPP
#define BILLCREATOR_HPP

#include "./fast-cpp-csv-parser/csv.h"
#include <QString>
#include <ctime>
#include <fstream>
#include <iostream>
#include <locale>
#include <podofo/podofo-base.h>
#include <podofo/podofo.h>
#include <string>
#include <vector>

class BillCreator
{
public:
    BillCreator();
    ~BillCreator();

    std::string lower(std::string sourceString);
    std::string asMajorCurrency(int amountWithDecimals);

    std::string asMinorCurrency(int amountWithDecimals);
    void checkStartWeek(int& startWeek, std::string billingKeys);
    bool hasMultipleEntries(std::vector<std::string> customerData, std::vector<std::vector<std::string>> allCustomers);
    std::vector<std::string> getChildList(std::string parentName,
        const std::vector<std::vector<std::string>>& allCustomers);
    std::vector<std::pair<int, int>> getSortedListOfGroups(std::string customerName,
        const std::vector<std::vector<std::string>>& priceData,
        const std::vector<std::vector<std::string>>& allCustomers);
    void addSpecialPaymentRow(std::string itemName,
        float paymentAmount,
        int rowNumber,
        int itemRowSpacer,
        PoDoFo::PdfPainterMM* painter);
    void createBillWithDescriptionAndSum(const std::vector<std::string>& customerData,
        std::vector<std::string> descriptions,
        std::vector<float> paymentAmounts,
        std::string addToFileName = "");
    void createBill(const std::vector<std::string>& customerData,
        const std::vector<std::vector<std::string>>& priceData,
        const std::vector<std::vector<std::string>>& allCustomers);
    void addBillRow(std::string itemName,
        int paymentAmount,
        int& totalPayment,
        int& itemRowSpacer,
        PoDoFo::PdfPainterMM* painter,
        int extraPaymentDays = 0);
    void defineFonts(PoDoFo::PdfStreamedDocument& document);
    void drawFooter(PoDoFo::PdfPainterMM* painter);
    void getFooterData(std::vector<std::string>& footerData);
    bool getCustomerData(std::vector<std::vector<std::string>>& customerData, const QString& filename);
    void getPriceData(std::vector<std::vector<std::string>>& priceData);
    int createBills(std::vector<std::vector<std::string>> allCustomers);

private:
    PoDoFo::PdfFont* pFont;
    PoDoFo::PdfFont* pCaptionFont;
    PoDoFo::PdfFont* pBoldFont;
    PoDoFo::PdfFont* pItalicFont;

    // sizes in millimeters
    int mLineSpacing = 4000;
    int mMargin = 25000; // Markers how the spreadsheet is interpreted
    int mCaptionXBox1 = 110000;
    int mCaptionYBox1 = 297000 - mMargin - 5000;
    int mContentXBox1 = 155000;
    int mCaptionXMainContent = mMargin;
    int mCaptionYMainContent = 297000 - 90000;
    int mContentMainContent = mCaptionYMainContent - 15000;
    int mColumn1X = mMargin;
    int mColumn2X = mMargin + 70000;
    int mColumn3X = mMargin + 120000;
    int mColumn3bX = mMargin + 125000;
    int mColumn4X = mMargin + 150000;
    int mCaptionXFooter = mMargin;
    int mCaptionYFooter = 75000;
    int mSpaceBetweenItems = 5000;
    int mBiggerSpacer = 15000;
    int mLogoX = mMargin;
    int mLogoY = 297000 - mMargin - 20000;
    float mImageScale = 0.5f;
};

#endif // BILLCREATOR_HPP
